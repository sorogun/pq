import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.plaf.metal.MetalScrollBarUI;

    public class MyScrollbarUI extends BasicScrollBarUI {

        Image imageThumb, imageTrack;
      JButton b = new JButton() {

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(0, 0);
            }

        };

       public MyScrollbarUI() {
            imageThumb = new ImageIcon("C:\\Users\\Sam\\Pictures\\waecphy\\scrool.png").getImage();
            imageTrack = new ImageIcon("C:\\Users\\Sam\\Pictures\\waecphy\\scrooler.png").getImage();
        }

        @Override
        protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
            g.setColor(Color.blue);
            ((Graphics2D) g).drawImage(imageThumb,
                r.x+c.getWidth()-imageThumb.getWidth(null), r.y,imageThumb.getWidth(null), r.height, null);
        }

        @Override
        protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
            ((Graphics2D) g).drawImage(imageTrack,
                 r.x+c.getWidth()-imageThumb.getWidth(null), r.y, imageThumb.getWidth(null), r.height, null);
        }
        @Override
  protected void installDefaults() {
        scrollBarWidth = 50;
      
    }
        @Override
        protected JButton createDecreaseButton(int orientation) {
            return b;
        }

        @Override
        protected JButton createIncreaseButton(int orientation) {
            return b;
        }
    }
