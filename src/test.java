import com.sam.last.Segment;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Random;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javafx.event.Event;
import javafx.event.EventHandler;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.plaf.basic.BasicScrollPaneUI;
import javax.swing.plaf.basic.BasicTextFieldUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import net.miginfocom.swing.MigLayout;
import sun.swing.DefaultLookup;

public class test extends JPanel implements EventHandler,KeyListener,ChangeListener {
    static AudioFormat af;
    static AudioInputStream ais;
    static DataLine.Info dil;
    static Clip sdl;
    static JTextArea field;
    static JTextField unquest;
    static JTextPane fiel;
    static JTextPane tpane;
    static Image rats;
    String before;
    JScrollPane nepa ;
   static int[] tib;
   static int[] bit;
   static BasicButtonUI bui;
    static JButton start;
    static boolean chess;
    JScrollBar lbar = new JScrollBar();
    static JButton dis;
    static JButton mark;
    static JButton exit;
    static JButton solute;
    static JButton next;
    static JButton prev;
    static JButton again;
    static JButton formula;
    static JLabel time;
    static ImageIcon icon =new ImageIcon(pq.cl.getResource("Cap.png"));
    static JLabel uquest;
    static JLabel page;
    static JLabel title,babbel;
    static JScrollPane scroll;
    static JScrollPane scroller;
    static JTextField hourfield , minfield , secfield;
    static JTextField pag;
    static boolean rock=false;
    static JButton connection;
    static Object waiting = new Object();
    static ArrayList<Integer>order= new ArrayList<>();
    static ArrayList<Integer>questions= new ArrayList<>();
    static ArrayList<Integer>indices= new ArrayList<>();
    static ArrayList<Integer>alpha= new ArrayList<>();
    static ArrayList<Integer>beta= new ArrayList<>();
    static ArrayList<String> slack = new ArrayList<>();
    static ArrayList<Integer> miss = new ArrayList<>();
    static ArrayList<Integer>bone = new ArrayList<>();
    static ArrayList<Integer> john = new ArrayList<>();
    static ArrayList<String> bolly = new ArrayList<>();
    static LinkedList<Integer> track = new LinkedList<Integer>();
    static ArrayList<Integer>sizes= new ArrayList<>();
    ArrayList<JTextPane>aer = new ArrayList<>();
    ArrayList<JTextArea> area = new ArrayList<>();
    ArrayList<JTextPane> aera = new ArrayList<>();
    static ArrayList<Integer> amber = new ArrayList<>();
    static ArrayList<String> file = new ArrayList<>();
    static ArrayList<String> life = new ArrayList<>();
    ArrayList<JRadioButton> buttons = new  ArrayList<>();
    static ArrayList<Integer> answers= new ArrayList<>();
    static ArrayList<Integer> th=new ArrayList<>();
    static ArrayList<Integer> eory=new ArrayList<>();
    GridBagConstraints gbc;
    ArrayList<ButtonGroup> bg = new  ArrayList<>();
    static Calendar cal;
    static boolean running;
    static volatile boolean come,submit=false;
    static boolean checks=false;
    static TreeSet<Integer> tes;
    final String the = "Solution:";
    static JPanel jpan;
    static JPanel JAPAN;
    static Timer timer;
    static volatile String netsolve;
    static long da;
    static int q;
    static int h;
    static int mun;
    static int cnt;
    static int ii;
    static int g;
    static BufferedImage mage = new BufferedImage(icon.getIconWidth(),icon.getIconHeight(),BufferedImage.TYPE_INT_ARGB);
    static happen happy=new happen();
    static volatile int tit,counter=0,timeline=0,btncnt=0;
    static volatile Color beauty = Color.WHITE;
    int act = 0;
    static double wrong ,perc;
    static double right;
    static int fin;
    static int i;
    static boolean network = false;
    static String load;
    static Thread thq;
    static JButton correct;
    static JButton star;
    static PrintWriter pw;
    public test() {
        bui = new BasicButtonUI();
        netsolve="";
        lbar.setUI(new MyScrollborUI());
        fiel = new JTextPane();
        fiel.setContentType("text/html");
        fiel.setEditable(false);
        rats = new ImageIcon(pq.cl.getResource("star.png")).getImage();
        nepa = new JScrollPane();
        nepa.setBorder(null);
        nepa.addKeyListener(this);
        nepa.setVerticalScrollBar(lbar);
        nepa.setHorizontalScrollBar(null);
        star = new JButton();
        star.setOnAction(this);
        star.addKeyListener(this);
        pq.cbi.setOnAction(this);
        pq.bic.setOnAction(this);
        pq.slide.setOnAction(this);
        addKeyListener(this);
        String text = "";
        tpane = new JTextPane();
        tpane.setContentType("text/html");
        tpane.setEditable(false);
        tpane.setText("");
       
        setLayout(new BorderLayout(10, 10));
        h = 1;
        pag = new JTextField(10);
        pag.addKeyListener(this);
        page = new JLabel("PAGE ");
        page.addKeyListener(this);
        jpan = new JPanel(new MigLayout());
        jpan.addKeyListener(this);
        jpan.setBackground(Color.white);
        JAPAN = new JPanel(new GridLayout(0, 5));
        JAPAN.addKeyListener(this);
        JAPAN.setBackground(Color.white);
        uquest = new JLabel(           );
        uquest.addKeyListener(this);
        uquest.setIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("step0003.png")).getImage()));
        uquest.setHorizontalAlignment(JLabel.RIGHT);
        unquest = new JTextField();
        unquest.addKeyListener(this);
        unquest.setEditable(false);
        scroller = new JScrollPane(unquest);
        scroller.setUI(new BasicScrollPaneUI());
        scroller.addKeyListener(this);
        scroller.setBorder(null);
        start = new JButton("");
        start.setToolTipText("CLICK TO START TIMER");
        start.setContentAreaFilled(false);
        start.addKeyListener(this);
        start.setIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("start.png")).getImage()));
        start.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("start1.png")).getImage()));
        start.setDisabledIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("start2.png")).getImage()));
        start.addActionListener(this);
        next = new JButton("");
        connection = new JButton(new ImageIcon(raw.mib));
        connection.setUI(bui);
        connection.setContentAreaFilled(false);
        connection.setToolTipText("CONNECTION STATUS");
        connection.addKeyListener(this);
        connection.addActionListener(this);
        next.setToolTipText("NEXT PAGE");
        next.addKeyListener(this);
        next.setIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("Next.png")).getImage()));
        next.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("next1.png")).getImage()));
        next.addActionListener(this);
        next.setUI(bui);
        next.setBackground(Color.WHITE);
        prev = new JButton();
        prev.setUI(bui);
        prev.addKeyListener(this);
        prev.setBackground(Color.white);
        prev.addActionListener(this);
        prev.setIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("prev.png")).getImage()));
        prev.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("prev1.png")).getImage()));
        exit = new JButton("");
        exit.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("back1.png")).getImage()));
        exit.addActionListener(this);
        exit.addKeyListener(this);
        formula = new JButton("");
        formula.setToolTipText("VIEW FORMULAS");
        formula.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().createImage(pq.cl.getResource("formula.png"))));
        formula.addActionListener(this);
        formula.addKeyListener(this);
        formula.setRolloverIcon(new ImageIcon(Toolkit.getDefaultToolkit().createImage(pq.cl.getResource("formula1.png"))));
        formula.setContentAreaFilled(false);
        mark = new JButton("");
        mark.setToolTipText("MARK YOUR WORK");
        mark.setContentAreaFilled(false);
        mark.addKeyListener(this);
        mark.setIcon(new ImageIcon(pq.cl.getResource("mark.png")));
        mark.addActionListener(this);
        mark.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("mark1.png")).getImage()));
        mark.setDisabledIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("mark2.png")).getImage()));
        exit.setIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("back.png")).getImage()));
        exit.setContentAreaFilled(false);
        exit.setToolTipText("BACK TO MAIN MENU");
        exit.addKeyListener(this);
        time = new JLabel();
        time.setToolTipText("TIME");
        time.addKeyListener(this);
        time.setIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("Timer.png")).getImage()));
        hourfield = new JTextField(5);
        hourfield.setUI(new BasicTextFieldUI());
        hourfield.setFont(new Font("Dialog", 0, 20));
        hourfield.addKeyListener(this);
        hourfield.setForeground(Color.white);
        hourfield.setEditable(false);
        hourfield.setHorizontalAlignment(JTextField.CENTER);
        minfield = new JTextField(5);
        minfield.addKeyListener(this);
        minfield.setHorizontalAlignment(JTextField.CENTER);
        minfield.setEditable(false);
        minfield.setUI(new BasicTextFieldUI());
        minfield.setFont(new Font("Dialog", 0, 20));
        minfield.setForeground(Color.white);
        secfield = new JTextField(5);
        secfield.addKeyListener(this);
        secfield.setHorizontalAlignment(JTextField.CENTER);
        secfield.setEditable(false);
        secfield.setUI(new BasicTextFieldUI());
        secfield.setFont(new Font("Dialog", 0, 20));
        secfield.setForeground(Color.white);
        solute = new JButton(new ImageIcon(new ImageIcon(pq.cl.getResource("pkey.png")).getImage()));
        solute.setDisabledIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("pkey2.png")).getImage()));
        solute.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("pkey3.png")).getImage()));
        solute.setContentAreaFilled(false);
        solute.addKeyListener(this);
        solute.addActionListener(this);
        again = new JButton(new ImageIcon(new ImageIcon(pq.cl.getResource("again2.png")).getImage()));
        again.setToolTipText("REDO THE TEST");
        again.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("again1.png")).getImage()));
        again.setDisabledIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("again.png")).getImage()));
        again.addActionListener(this);
        again.addKeyListener(this);
        dis = new JButton();
        dis.setToolTipText("CORRECT THIS ANSWER");
        dis.setIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("edit.png")).getImage()));
        dis.setDisabledIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("edit1.png")).getImage()));
        dis.setContentAreaFilled(false);
        dis.addKeyListener(this);
        dis.addActionListener(this);
        title = new JLabel("");
        babbel = new JLabel("");
        babbel.setLayout(null);
        again.setUI(bui);
        again.setContentAreaFilled(false);
       jpan.add( start,"gapleft 100,gapright 40");
       jpan.add( time,"gapright 40");
       jpan.add( hourfield,"w 35,h 40,gapleft 10,gapright 0");
       jpan.add( minfield,"w 35,h 40,gapright 0");
       jpan.add( secfield,"w 35,h 40,gapright 60");
       jpan.add( mark,"gapright 50");
       jpan.add( exit,"gapright 50");
       jpan.add( page,"gapright 20");
       jpan.add( pag,"gapright 50");
       jpan.add(formula,"gapright 50");
       jpan.add(connection);
       JAPAN.add(uquest);
       JAPAN.add(scroller);
       JAPAN.add(again);
       JAPAN.add(solute);
       JAPAN.add(dis);
       
        cal = Calendar.getInstance();
        
        
        
     
        
        
        
        timer = new Timer(30, this);
        field = new JTextArea();
        
       setBackground(Color.WHITE);
    }

    public void acting(int veep) {
      
            
          
                fiel.removeAll();
                rock=false;
                nepa.getViewport().setView(aera.get(veep));
                h = veep;
                this.validate();
                 if(h<=th.size()&&th.get(h-1)!= -1){
        solute.setEnabled(true);}
        else { solute.setEnabled(false);}
            
      
            
        }
    

    public void Components() {
        if(network){connection.setVisible(true);}
        else{connection.setVisible(false);}
        field.setText("");
        tib = new int[sizes.size()+1];
        bit = new int[sizes.size()+1];
        h=1;
        solute.setEnabled(false);
        add(jpan, "North");
        add( JAPAN, "South");
        add( prev, "West");
        add( next, "East");
        add(nepa,BorderLayout.CENTER);
       nepa.getViewport().setView(aera.get(h));
        this.doLayout();
        nepa.doLayout();
        nepa.validate();
        validate();
        
        
        if (!network) {
            start.setEnabled(true);
            mark.setEnabled(false);
        } else {
            start.doClick();
            start.setEnabled(false);
            mark.setEnabled(true);
            
        }
        again.setEnabled(false);
       
    if(file.size()>1){dis.setEnabled(false);}
    else{dis.setEnabled(true);}}
    
    
    public void cleanup(){
        area.clear();
        aera.clear();
        aer.clear(); 
        buttons.clear();
        bit=null;
        tib=null;
        order.clear();
        sizes.clear();
        bg.clear();
        file.clear();
        life.clear();
        miss.clear();
        bolly.clear();
        slack.clear();
        questions.clear();
        answers.clear();
        amber.clear();
        john.clear();
        track.clear();
        indices.clear();
        alpha.clear();
        beta.clear();
        bone.clear();
        th.clear();
        eory.clear();
       
    }

    public void undone() {
unquest.setText("");
        for (int y = 1; y <bg.size(); ++y) {
          
            unquest.setText(unquest.getText()+" "+y);
            
        }
        
       unquest.setText(unquest.getText()+" ");
            
    }

    public static int  getColor() {
        if (perc > 80) {
            return 5;
        }
        else if (perc >= 60 ) {
            return 4;
        }
        else if (perc >= 40) {
            return 3;
        }
        else if (perc >=20) {
           return 2;
        }
        return 0;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        }

    static String data(JTextArea a, int i, int j) {
       return a.getText().substring(i, j);
    }

    static int seti(int s, int t) {
        Random ramdom = new Random();
        s = ramdom.nextInt( t );
        return s;
    }

    

   

    static int getend(JTextArea field) {
                int num = 102;
        do{num--;}
        while(field.getText().indexOf(String.valueOf(num).concat(". ")) <= 0);
            return num;
         
    }

    public void timeup() {
        JOptionPane.showMessageDialog(null, "TIME UP");
        mark.doClick();
    }

    @Override
    public void actionPerformed(ActionEvent e) {}
   
    public void mark() {
        boolean jk = true;
        boolean hb = false;
        int rit = 0;
        int rr = 0;
       for(int hup=0;hup<order.size();++hup){
           
                rit =answers.get(hup);
                rr =  sizes.get(hup);
               
 if(rr>0){              
 ButtonGroup bug = bg.remove(1);
                Enumeration<AbstractButton> aba =  bug.getElements();
                while (aba.hasMoreElements()) {
                    aba.nextElement().setEnabled(false);
                }
                    
                    
       
      
    if(rit>=0){ HTMLDocument docc = (HTMLDocument) aer.get(order.get(hup)+rit).getStyledDocument();
      try{ 
          String   hes =  docc.getText(0, docc.getLength());   
     loadImages(aer.get(order.get(hup)+rit),hes,"#054d19","white","15","bold",new HTMLDocument());
     if(bit[hup+1]!=0){
     if(rit!=tib[hup+1]){
     wrong+=1.0;
     docc = (HTMLDocument) aer.get(bit[hup+1]-1).getStyledDocument();
     hes =  docc.getText(0, docc.getLength());   
     loadImages(aer.get(bit[hup+1]-1),hes,"#990000","white","15","bold",new HTMLDocument());
     
     }else{right+=1.0;}}
     else{wrong+=1.0;}
    
    
        }   catch (BadLocationException ex) {
            //    Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
            }
 }
    }else{ }
 }   
        bg.clear();
        answers.clear();
        tib=null;
        bit=null;
        order.clear();
        result();
    
    }
   





   public static void treat(boolean ladi ,int max,String l,ArrayList ... hoc){
       int indi =0;
     for(String poo:l.split("`")){
               if(indi==0){
               if(poo.compareTo("n")==0){
               hoc[0].add(Integer.parseInt(l.split("`")[1]));
               hoc[1].add(Integer.parseInt(l.split("`")[2]));
               poo=null;
               l=null;
               if(ladi){
               hoc[2].add(99999);}
                                  break; 
               }
               hoc[2].add(Integer.parseInt(poo));
               }
               else{ if(!poo.isEmpty()){
                   if(max==0){break;}
               indices.add(Integer.parseInt(poo));}
               } indi++;
               poo=null;}  }
   
   public static void addAnswers(String hop){
        int pasre = Integer.parseInt(hop.split("`")[0].trim());
        int pasr =  Integer.parseInt(hop.split("`")[1].trim());        
        int pas =  Integer.parseInt(hop.split("`")[2].trim());
        int past = Integer.parseInt(hop.split("`")[3].trim());
        if(past==0){
                answers.add(pasre);
                th.add(pasr);
                eory.add(pas);}
        else{        
        }
hop=null;}
   
    public static void produce(boolean kill){
       if(kill){ 
           for(int buj =0;buj<mun;buj++){
            boolean vun =true;
        int rut =0;int py=0;
        rut = seti(rut,test.file.size());
        
       
        py=seti(py,miss.get(rut)-1)+1;
        String wanheda="";String humml="";
        if(network){
         wanheda = Segment.getText(slack.get(rut),null,0,true,py)+" ";
         humml=Segment.getText(slack.get(rut),null,0,true,(py+1));}
        else{
       wanheda = Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(rut))),0,true,py)+" ";
        humml=Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(rut))),0,true,(py+1));
        }
         
        if(humml.contains("n`")){
     vun=false;
      }
       String l = wanheda.split("```")[0].split("``")[0];
           String to=wanheda.split("```")[0].split("``")[1];
           String no = wanheda.split("```")[1];
            String ref = wanheda.split("```")[2];
           String rob = wanheda.split("```")[0].split("``")[2];
            if(!ref.trim().isEmpty()){
           py=Integer.parseInt(ref.trim());
           if(network){
           wanheda = Segment.getText(slack.get(rut),null,0,true,py)+" ";
           }
           else{
           wanheda = Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(rut))),0,true,py)+" ";}
           l = wanheda.split("```")[0].split("``")[0];
           to=wanheda.split("```")[0].split("``")[1];
           no = wanheda.split("```")[1];
           rob = wanheda.split("```")[0].split("``")[2];
              
         }
         if(wanheda.contains("n`")&&no.trim().length()<2){
         vun=false;}  
         
        
          if(vun){
            amber.add(rut);
            track.add(Integer.parseInt(rob));
            sizes.add(Integer.parseInt(to));
            treat(false,1,l,bone,john,bone);
            
            String   hop="";
            if(network){
hop=Segment.getText(bolly.get(rut),null,0,true,py);
}
            else{
hop=Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,bolly.get(rut))),0,true,py);
}
            addAnswers(hop);       
            if(!no.trim().isEmpty()){
            buj--;
            for(String buy:no.split(",")){
              
            int eme = Integer.parseInt(buy.split("@")[0].trim());
            if(Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(rut))),0,true,(eme+1)).trim().length()==0){            break;}
            track.add(Integer.parseInt(buy.split("@")[1].trim()));
            amber.add(rut);
             if(network){hop=Segment.getText(bolly.get(rut),null,0,true,eme);
}else{hop=Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,bolly.get(rut))),0,true,eme);
}addAnswers(hop);   
             if(network){wanheda=Segment.getText(slack.get(rut),null,0,true,py);
}else{wanheda=Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(rut))),0,true,py);
}l = wanheda.split("```")[0].split("``")[0];
            to=wanheda.split("```")[0].split("``")[1];
            sizes.add(Integer.parseInt(to));
            treat(false,1,l,bone,john,bone);
if(network){            wanheda = Segment.getText(slack.get(rut),null,0,true,(eme+1));
}else{
    wanheda = Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(rut))),0,true,(eme+1));
}            
           l = wanheda.split("``")[0];
            treat(false,0,l,bone,john,john);
            buj++;}
           }else{
        if(network){            wanheda = Segment.getText(slack.get(rut),null,0,true,(py+1));
}else{
    wanheda = Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(rut))),0,true,(py+1));
}            
   l = wanheda.split("```")[0].split("``")[0];
          treat(false,0,l,bone,john,john);
             }}
           l=null;    
           to=null; 
           wanheda = null;
               
               }}
       else{
           if(!network){
       if(bolly.get(0).toString().contains("oqa")){
        String hop=Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,bolly.get(0))),0,true);
       for(String huk:hop.split("\n")){
       addAnswers(huk);
        }}
       
           }
       else{
           if(bolly.get(0).toString().contains("oqa")){
        String hop=Segment.getText(bolly.get(0),null,0,true);
       for(String huk:hop.split("\n")){
       addAnswers(huk);
        }}}
        
        String hop=Segment.getText(bolly.get(0),null,0,true);
        String sdk="";
       if(!network){ sdk = Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(0))),0,true).trim();        }
       else{ sdk = Segment.getText(slack.get(0),null,0,true).trim();}
       
        int oi=1;
        for(String huk:sdk.split("\n")){
           if(!huk.isEmpty()){
           String l = huk.split("``")[0];
           String to=huk.split("``")[1];
           if(!network){
           if(Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,slack.get(0))),0,true,oi+1).contains("n`")&&!l.contains("n`")) {
           }
           else{
               sizes.add(Integer.parseInt(to));}}
           else{
               if(Segment.getText(slack.get(0),null,0,true,oi+1).contains("n`")&&!l.contains("n`")) {
           }
           else{
               sizes.add(Integer.parseInt(to));}}
           treat(true,1,l,alpha,beta,questions);   
             to=null;  
   
            }
       oi++; }
        sdk=null;
        }
   
   
   }

    public void createquestions() {
       
       checks=false;
       g = 0;
       StringBuilder sb = new StringBuilder();
       String spag="";
       boolean repeat = false;
        int index = 0;
        int number = 0;
        int index3 = 0;
        int index2=0;
        int index4 = 0;
        int index5 = 0;
       
     
        

        
       
        boolean hig = false;
        boolean cunt = true;
        int rav=0;
        wrong = 0.0;
        right = 0.0;
        timer.stop();
        btncnt = 1;
        area.add(new JTextArea());
        aera.add(new JTextPane());
        bg.add(new ButtonGroup());
        JTextArea kanye =new JTextArea();
        q = 0;
        int itt = 0;
        boolean gr = false;
        if(network){produce(running);}
        else{produce(running);}
         if (!running) {
        
        mun = sizes.size();
if(!network)
{field.setText(Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,file.get(0))),1,true));}
else{    
    field.setText(Segment.getText(file.get(0),null,1,true));
} 
         }
        else{
        mun=amber.size();}
        for (int it = 1; it <=mun; ++it) {
            ++number;
            chess=true;
           
                
             
                ++q;
          if (!running) {
           kanye.setText("");
           JTextPane drake = new JTextPane();
           aera.add(new JTextPane());
           drake.setContentType("text/html");
           drake.addKeyListener(this);
           
        
            try{index = questions.get(q-1);}
            catch(Exception kol ){break;}
            if(q==questions.size()){index2=-1;}
          else{index2 = questions.get(q);}
          
            if (index >= 0) {
             sb.delete(0,sb.length());
             if(index==99999){
             index = alpha.remove(0);index2=beta.remove(0);
             questions.remove(q-1); q--;chess=false;
                }
             if(index2==99999){
             index = alpha.remove(0);index2=beta.remove(0);
                questions.remove(q);questions.remove(q-1);q--;chess=false;
              }
             
             if (index2 > 0 ) {
                 sb.append( data(field, index, index2));
                   
                kanye.setText(sb.toString());
              
                   
                } else if (index2 < 0 ) {
                   sb.append( data(field, index, field.getText().length()));
                    kanye.setText(sb.toString());
                } 
                
                options(kanye, drake,btncnt,it,number-1,index);
                
            }else {
                it = mun;
            }
            try {
                
        drake.setTransferHandler(null);
        loadImages(drake,kanye.getText(),"white","black","25","normal",new HTMLDocument());
        drake.setEditable(false);
        aera.add(it,drake);
            }
            catch (Exception e) {
            //                     e.printStackTrace();
           }}
          else{
          field.setText(Segment.getText(file.get(amber.get(q-1)),null,1,true));
           kanye.setText("");
           JTextPane drake = new JTextPane();
           aera.add(new JTextPane());
           drake.setContentType("text/html");
           drake.addKeyListener(this);
           
        
           index = bone.get(q-1);
           index2 = john.get(q-1); if (index >= 0) {
             sb.delete(0,sb.length());
             if (index2 > 0 ) {
                  if(!network){  }
                  else{}
                   if(!data(field, index, index2).contains(track.getFirst()+". "))
                   {number--;
                   }
                   
                kanye.setText( data(field, index, index2));
              
                   
                } else if (index2 < 0 ) {
                    if(!network){ }
                  else{}
                    
                    
                     kanye.setText(data(field, index, field.getText().length()));
                } 
                options(kanye, drake,btncnt,it,q-1,index);
                sb.append(kanye.getText() );
                sb = newText(sb, number, track.pop());
                     kanye.setText(sb.toString());
                
            }else {
                it = mun;
            }
            try {drake.setTransferHandler(null);
        loadImages(drake,kanye.getText(),"white","black","25","normal",new HTMLDocument());
        drake.setEditable(false);
        aera.add(it,drake);
            }
            catch (Exception e) {
                                 //e.printStackTrace();
                it = mun -= 3;
            }
          }}
       
        
        Components();
        if (!network &&answers.isEmpty()) {
            mark.setEnabled(false);
        } else {
            mark.setEnabled(true);
        }
       undone();
    }

    StringBuilder newText(StringBuilder sb, int it, int q) {
        String abs = sb.toString().replaceFirst(Pattern.quote(""+q+". "), ""+it+". ");
        
        sb.delete(0, sb.length());
        sb.append(abs);
        return sb;
    }



    public void createoptions(int index, int index2, JTextPane tj, JTextArea jt, ButtonGroup i, int g, int kk,int numb,JTextPane drake) {
        tj.setLayout(null);
        buttons.add(new JRadioButton());
        drake.setContentType("text/html");
        if(kk%2 == 1){
        drake.setBounds(600, 400 + (kk/2) * 73, 450, 73);}
        else{         drake.setBounds(70, 400 + (kk/2) * 73, 450, 73);}
        
        buttons.get(g).setActionCommand(String.valueOf(1+g));
        buttons.get(g).setName(numb+"");
        buttons.get(g).setIconTextGap(kk);
        buttons.get(g).addChangeListener(happy);
        buttons.get(g).addKeyListener(this);
        drake.addKeyListener(this);
        drake.setEditable(false);
        drake.setBorder(BorderFactory.createEtchedBorder());
        i.add( buttons.get(g));
        buttons.get(g).setBackground(Color.white);
        tj.add( buttons.get(g));
        buttons.get(g).setBounds(drake.getBounds().x-60,  drake.getY(), 60, 60);
        
       
               
        if (index2 > 0) {
        loadImages(drake,data(jt, index, index2),"white","black","15","bold",new HTMLDocument());
         }
        aer.add(drake); 
        tj.add(aer.get(g));
       }

    private void loadImages( JTextPane tj,String jt,String bColor,String Color,String size,String weight,HTMLDocument docc) {
        tj.setStyledDocument(docc);
        StyleSheet styles = docc.getStyleSheet();
        SimpleAttributeSet sas = new SimpleAttributeSet();
        
        styles.addAttributes(styles.getEmptySet(), sas);
     styles.addRule("body {background-color:"+bColor+";color:"+Color+"; font-family:verdana;font-size:"+size+";font-weight:"+weight+"; margin:0px;}");
     
    
        
        HTMLEditorKit kite = (HTMLEditorKit)tj.getEditorKit();
       
        if (jt.indexOf("<im>") > 0) {
           
            if( jt.indexOf("</im>")<0){}
            String yup = jt.substring(jt.indexOf("<im>")+4, jt.indexOf("</im>"));
            jt=(jt.replaceFirst("<im>", ""));
            jt=(jt.replaceFirst("</im>", ""));
            try {
                kite.insertHTML(docc,docc.getLength(),jt.substring(0,jt.indexOf(yup)),0,0,null);
            } catch (BadLocationException ex) {
            } catch (IOException ex) {
            //    Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
            }
            jt=(jt.substring(jt.indexOf(yup)+yup.length(),jt.length()));
            ImageIcon newimage = new ImageIcon(/*Toolkit.getDefaultToolkit().getImage(pq.cl.getResource(yup))*/subjects.getData(subjects.barter,yup));
            if(newimage.getIconHeight()>200 && tj.getComponentCount()>4){
            double count = (double)200/(double)newimage.getIconHeight();
            Image oldimage = newimage.getImage().getScaledInstance((int)(newimage.getIconWidth()*count),200, Image.SCALE_SMOOTH);
            newimage=new ImageIcon(oldimage);
            } 
            tj.insertIcon(newimage);
            
            loadImages(tj,jt,bColor,Color,size,weight,docc);
      }
        else{try {
           kite.insertHTML(docc,docc.getLength(),jt,0,0,null);
           } catch (BadLocationException | IOException ex) {
           //     Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
       }

    private synchronized void options(JTextArea jt, JTextPane tj, int i,int ith,int real,int xes) {
       boolean checker = true;
        int index2;
        int kk;
        int index;
    bg.add(new ButtonGroup());
   
        if (chess&&sizes.get(real) > 0) {
            for (kk = 0; kk < sizes.get(real); ++kk) {
                index = indices.get(g);
                  if(kk+1==sizes.get(real)){
                index2=indices.remove(g+1);}
                else{index2 = indices.get(g+1);
                }
                createoptions(index, index2, tj, field,  bg.get(i), g, kk,ith,new JTextPane());
                ++g;
                
            }order.add(g-sizes.get(real));
            
            btncnt++;
           
            jt.setText( data(field, xes, indices.get(order.get(real))));
        }
        else{jt.setText( jt.getText());
          order.add(0);}
        tj.setName(ith+"``"+i);
    
             }
    
    static String up(String per){
   
for (int ar=0;ar<per.length();ar++){
switch (per.charAt(ar))
{
case '0':per=per.replace(per.charAt(ar),'º');break;
case '1':per=per.replace(per.charAt(ar),'¹');break;
case '2':per=per.replace(per.charAt(ar),'²');break;
case '3':per=per.replace(per.charAt(ar),'³');break;
case '4':per=per.replace(per.charAt(ar),'⁴');break;
case '5':per=per.replace(per.charAt(ar),'⁵');break;
case '6':per=per.replace(per.charAt(ar),'⁶');break;
case '7':per=per.replace(per.charAt(ar),'⁷');break;
case '8':per=per.replace(per.charAt(ar),'⁸');break;
case '9':per=per.replace(per.charAt(ar),'⁹');break;
case '-':per=per.replace(per.charAt(ar),'⁻');break;
case '+':per=per.replace(per.charAt(ar),'⁺');break;}
}
return per;}
    
    static String rep(int erp){
        String per =""+erp;
for (int ar=0;ar<per.length();ar++){
switch (per.charAt(ar))
{
case '0':per=per.replace(per.charAt(ar),'₀');break;
case '1':per=per.replace(per.charAt(ar),'₁');break;
case '2':per=per.replace(per.charAt(ar),'₂');break;
case '3':per=per.replace(per.charAt(ar),'₃');break;
case '4':per=per.replace(per.charAt(ar),'₄');break;
case '5':per=per.replace(per.charAt(ar),'₅');break;
case '6':per=per.replace(per.charAt(ar),'₆');break;
case '7':per=per.replace(per.charAt(ar),'₇');break;
case '8':per=per.replace(per.charAt(ar),'₈');break;
case '9':per=per.replace(per.charAt(ar),'₉');break;
case '-':per=per.replace(per.charAt(ar),'⁻');break;
case '+':per=per.replace(per.charAt(ar),'⁺');break;}
}
return per;}
  
    
   
    private static void toFile(File f , String love,int io ,FileOutputStream os ,boolean past){
  PrintWriter  pwint = null;
  pwint = new PrintWriter(os);
  if(past)
  { if(love!=null){
 for (String write :love.split("\n")){
 pwint.println(write);}
 pwint.flush();
 }
  else{
      pwint.println(io+"\n");
      pwint.flush();
 }
  love =null;}
  else{try {
      os.close();
      } catch (IOException ex) {
      //    Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
      }
}
               
   }



    public static void result() {
        if((right+wrong)==0){}else{
            babbel.removeAll();
            babbel.setHorizontalAlignment(JLabel.CENTER);
            babbel.setVerticalAlignment(JLabel.CENTER);
            pq.T.removeAll();
            pq.T.validate();
            pq.T.repaint();
            DecimalFormat df = new DecimalFormat("#.###");
            df.setRoundingMode(RoundingMode.CEILING);
            perc = right / (right + wrong) * 100;
            perc = Double.valueOf(df.format(perc));
            Graphics2D g3 = mage.createGraphics();
            g3.setStroke(new BasicStroke(2.0f));
            g3.setBackground(Color.white);
            g3.clearRect(0, 0, mage.getWidth(), mage.getHeight());
            g3.drawImage(icon.getImage(), 0, 0, null);
            g3.setColor(Color.black);
            g3.setFont(new Font(Font.SERIF,Font.BOLD, 35));
            
            new Thread(){
            public void run(){
                try {
            g3.drawImage(new ImageIcon(pq.cl.getResource("score.png")).getImage(), 100, 100, null);
            g3.drawString((int)right+"/"+(int)(right+wrong),350,140);
            babbel.setIcon(new ImageIcon(mage));
            Thread.sleep(500);
            g3.drawImage(new ImageIcon(pq.cl.getResource("perc.png")).getImage(), 100, 200, null);
            g3.drawString(perc+"%", 350, 240);
            babbel.setIcon(new ImageIcon(mage));
            Thread.sleep(500);
            for(int toy=0;toy<getColor();toy++){
            g3.drawImage(rats, 100+(toy*100),500, null);
            babbel.setIcon(new ImageIcon(mage));
            Thread.sleep(500);
            }
            g3.dispose();
                } catch (InterruptedException ex) {
                //    Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
                }
            
            }}.start();
       String safe = subjects.choice;
       babbel.setIcon(new ImageIcon(mage));
       String uber = pq.prop.get(safe,"");
       if(uber.split(",").length>19){
       uber=uber.substring(uber.indexOf(",")+1);
       }
       pq.prop.put(safe,uber+ perc+"%,");
      come = false;
       if(wrong > 0.0){correct = new JButton(new ImageIcon(new ImageIcon(pq.cl.getResource("correct.png")).getImage())); 
       correct.setRolloverIcon(new ImageIcon(new ImageIcon(pq.cl.getResource("correct1.png")).getImage()));
       correct.setContentAreaFilled(true);
       correct.setBackground(Color.WHITE);
       correct.setUI(bui);}
       else {correct = new JButton("BACK");}
        if (right > 0.75 * (right + wrong)) {
         
        }
         correct.setBounds(600, 500, 200, 70);
         if(!network){
         babbel.add( correct);}
         else{
             synchronized(subjects.oba){
         subjects.oba.notifyAll();}
         }
         pq.T.add(babbel,BorderLayout.CENTER);
         pq.T.validate();
         pq.T.repaint();
        correct.addActionListener(pq.T);
        }    }
    
    

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
       
             act = e.getKeyCode();
            if ( act == 10 && pag.isFocusOwner()) {
            try{
                 int veep = Integer.valueOf(pag.getText());
                if (veep > mun) {
                    pag.setText(h+"");
                } else {
                   
                     acting(veep);
               }}
                catch(Exception noir){}  act = 0;
            }
                
            else if ( act == 37 && !pag.isFocusOwner()) {
                prev.doClick(10);
            } else if ( act == 39 && !pag.isFocusOwner()) {
                next.doClick(10);
            } else if ( act == 40) {
                
                  int love = Integer.valueOf(aera.get(h).getName().split("``")[0]);
                   int rent = Integer.valueOf(aera.get(h).getName().split("``")[1]);
                   if(sizes.get(love-1)>0){
                   int itex = bit[love];
                   int high =order.get(love-1);
                   int jew  = tib[love];
                       if(itex==0){
                    buttons.get(high).setSelected(true);
                    }
                   else{
                   
                   if(jew<bg.get(rent).getButtonCount()-1){
                   buttons.get(itex).setSelected(true);
                }
                   }}
                   
                  
        } else if ( act == 38) {
                   int love = Integer.valueOf(aera.get(h).getName().split("``")[0]);
                   int rent = Integer.valueOf(aera.get(h).getName().split("``")[1]);
                   if(sizes.get(love-1)>0){ int itex = bit[love];
                   int jew  = tib[love];
                   int high =order.get(love-1);
                   if(itex==0){
                   buttons.get(high+1).setSelected(true);
                   }
                   else{
                   if(jew>0){
                   buttons.get(itex-2).setSelected(true);}}
       
        }  
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider s = (JSlider)e.getSource();
        FloatControl volume = (FloatControl)sdl.getControl(FloatControl.Type.MASTER_GAIN);
        int x = s.getValue();
        if (x != 0) {
            volume.setValue(x + 1);
        } else {
            volume.setValue(-10000.0f);
        }
        pq.prefs.putInt("volume", x);
    }

    static {
        running = false;
        come = true;
        h = 1;
        mun = 0;
        ii = 0;
        g = 0;
        tit = 0;
        wrong = 0.0;
        right = 0.0;
    }

    @Override
    public void handle(Event e) {
    
        if (e.getSource() == timer) {
          int remaining;  
            cal = Calendar.getInstance();
          if(network){
          subjects.cale = Calendar.getInstance();
          int calc = (int)((cal.getTimeInMillis()-da) / 1000);
            remaining= timeline-calc ;
            if(remaining<=0){
            timer.stop();
            mark();}
            
          if((double)((double)remaining/subjects.span)>0.5){
          hourfield.setBackground(subjects.GREEN);minfield.setBackground(subjects.GREEN);secfield.setBackground(subjects.GREEN);}
          else if((double)((double)remaining/subjects.span)>0.3){
          hourfield.setBackground(subjects.ORANGE);minfield.setBackground(subjects.ORANGE);secfield.setBackground(subjects.ORANGE);}
          else{  hourfield.setBackground(subjects.RED);minfield.setBackground(subjects.RED);secfield.setBackground(subjects.RED);   }
                       
       }
          else{
            int calc = (int)((cal.getTimeInMillis() - da) / 1000);
            remaining=fin - calc;
            
            if (calc >= fin) {
                 timeup();
                timer.stop();
            }
        }
          hourfield.setText(remaining/3600+"");
     minfield.setText((remaining%3600)/60 +"");
     secfield.setText(((remaining)%3600)%60+"");
        }
        else if (e.getSource()==solute){
            
            
              if (rock ==false) {
                  if(running){
                loadImages(fiel,Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,life.get(amber.get(h-1)))),1,false,th.get(h-1),eory.get(h-1)),"white","black","25","",new HTMLDocument());
                  }else{
                loadImages(fiel, Segment.getText(null,new ByteArrayInputStream(subjects.getData(subjects.barter,life.get(0))),1,false,th.get(h-1),eory.get(h-1)),"white","black","25","",new HTMLDocument());
                } fiel.setCaretPosition(0);
                fiel.setLayout(null);
               nepa.getViewport().setView(fiel);
                rock=true;
            } else {
                fiel.removeAll();
             nepa.getViewport().setView(aera.get(h));
                rock=false;
            }
            nepa.validate();
            validate();
            repaint();
       }
         
        else if (e.getSource() == again) {
        cleanup();
        //JOptionPane.showMessageDialog(null,subjects.filename);
        if(!running){String[] deta = subjects.filename.split("`");
        test.file.add(deta[0]);
        test.slack.add(deta[1]);
        test.life.add(deta[2]);
        test.bolly.add(deta[3]);
        deta=null;}
        else{
        subjects.loadRand(subjects.line);
        }
        
        this.removeAll();
        this.createquestions();
        this.validate();
        } 
        else if (e.getSource() == correct) {
    
         
            come = true;
            this.removeAll();
            Components();
            this.repaint();
            if(!network)
            {     again.setEnabled(true);}
            else{again.setEnabled(false);}
            start.setEnabled(false);
            this.validate();
        }
        else if (e.getSource() == pq.cbi) {
            if (pq.cbi.isSelected()) {
                pq.prefs.putInt("timer", 1);
            } else {
                pq.prefs.putInt("timer", 0);
            }
        } 
        else if (e.getSource() == pq.bic) {
            if (pq.bic.isSelected()) {
                pq.prefs.putInt("animate", 1);
                pq.animate = true;
            } else {
                pq.prefs.putInt("animate", 0);
                pq.animate = false;
            }
        } 
        else if (e.getSource() == start) {
            pq.bar.setVisible(false);
            da = Calendar.getInstance().getTimeInMillis();
            timer.start();
            start.setEnabled(false);
        } else if (e.getSource() == prev) {
            if (h >= 2) {
                fiel.removeAll();
            rock=false;
                h--; nepa.getViewport().setView(aera.get(h));
                  nepa.validate();
               
                if(h<=th.size()&&th.get(h-1)!=-1){
                solute.setEnabled(true);}
                else { solute.setEnabled(false);}
            } else {
                repaint();
            }
        } 
        else if (e.getSource() == exit) {
            h = 1;
            g = 0;
            wrong = 0.0;
            right = 0.0;
            timer.stop();
            cleanup();
            pq.tub.doClick();
            network=false;
        } 
        else if (e.getSource() == next) {
            if (h < mun) {
                fiel.removeAll();
             rock=false;
                h++;
                nepa.getViewport().setView(aera.get(h));
             nepa.validate();
                          if(h<=th.size()&&th.get(h-1)!= -1){
            solute.setEnabled(true);}
        else { solute.setEnabled(false);}
            } else {
               repaint();
            }
        } 
        else if (e.getSource() == star) {
      pq.bar.setVisible(false);
            this.removeAll();
        createquestions();
           validate();
          if(network){
    
       pq.move(pq.nine, pq.sub, "right", this, false,null);
      
        } 
        else{}
      } 
        else if (e.getSource() == mark) {
          mark(); 
         
            mark.setEnabled(false);
           
        } 
        else if (e.getSource() == formula) {
            JDialog dg = new JDialog();
            JPanel ane = new JPanel(new BorderLayout());
            ane.add(new JScrollPane(tpane));
            dg.setContentPane(ane);
            dg.setModal(true);
            dg.setIconImage(new ImageIcon(pq.cl.getResource("pq1.png")).getImage());
            dg.setDefaultCloseOperation(2);
            dg.setSize(500, 600);
            dg.setResizable(false);
            dg.setVisible(true);
        }
        else if(e.getSource()==dis){
      String quest = file.get(0);
      JTextArea jat = new JTextArea(8,30);
     
      jat.setLineWrap(true);
      
      int outy = JOptionPane.showConfirmDialog(null,new JScrollPane(jat),"CORRECT THIS SOLUTION .STATE YOUR REASON(S) IF POSSIBLE",JOptionPane.OK_CANCEL_OPTION);
      if(outy == JOptionPane.OK_OPTION){
          String sol = jat.getText();
          if(sol.isEmpty()){}else{
      String fol = quest.substring(0, 7);
      if(sol.length()>1000){
     sol = sol.substring(0, 1000);}
      sol= sol.replaceAll(" ", "~");
     sol= sol.replaceAll("\n", "7@@");
     sol=sol.replaceAll("&", "~");
String west = "http://quizaq.com/connect.php?fol="+fol+"&quest="+quest+"~page~"+h+"&sol="+sol;
      try {
                java.net.URL conne = new java.net.URL(west);
                
                java.net.URLConnection nect = conne.openConnection();
                nect.setDoOutput(true);
                PrintStream ps = new PrintStream (nect.getOutputStream());
                ps.print("fol="+fol);
                ps.print("&quest="+quest);ps.print("&sol="+sol);
                nect.getInputStream();
                ps.close();
                JOptionPane.showMessageDialog(null,"Thank You For Your FeedBack");
                
            } catch (MalformedURLException ex) {
             //   Logger.getLogger(test.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
              pq.subs.put("unsent", pq.subs.get("unsent", "").concat("<newjk>"+west));
          }}}
        }
        else if(e.getSource()==connection){
        subjects.join.doClick();}
        
    }
    static class MyScrollborUI extends BasicScrollBarUI {
        private Image imageThumb, imageTrack;


        MyScrollborUI() {
            imageThumb = new ImageIcon(pq.cl.getResource("scro.png")).getImage();
            imageTrack = new ImageIcon(pq.cl.getResource("screen.png")).getImage();
        }
        @Override
protected void setThumbBounds(int x, int y, int width, int height)
    {
        /* If the thumbs bounds haven't changed, we're done.
         */
        if ((thumbRect.x == x+width-imageThumb.getWidth(null)) &&
            (thumbRect.y == y) &&
            (thumbRect.width == width) &&
            (thumbRect.height == height)) {
            return;
        }

       
        int minX = Math.min(x, thumbRect.x);
        int minY = Math.min(y, thumbRect.y);
        int maxX = Math.max(x + width, thumbRect.x + thumbRect.width);
        int maxY = Math.max(y + height, thumbRect.y + thumbRect.height);

        thumbRect.setBounds(x+width-imageThumb.getWidth(null), y, width, height);
        scrollbar.repaint(minX, minY, maxX - minX, maxY - minY);

        setThumbRollover(false);
    }        
  @Override
   protected void layoutVScrollbar(JScrollBar sb)
    {sb.setSize(imageThumb.getWidth(null),sb.getHeight());
        Dimension sbSize = sb.getSize();
        Insets sbInsets = sb.getInsets();
        sb.setBackground(Color.WHITE);

        int itemW = sbSize.width - (sbInsets.left + sbInsets.right);
        int itemX = sbInsets.left;
      
        /* Nominal locations of the buttons, assuming their preferred
         * size will fit.
         */
        boolean squareButtons = DefaultLookup.getBoolean(
            scrollbar, this, "ScrollBar.squareButtons", false);
        int decrButtonH = squareButtons ? itemW :
                          decrButton.getPreferredSize().height;
        int decrButtonY = sbInsets.top;

        int incrButtonH = squareButtons ? itemW :
                          incrButton.getPreferredSize().height;
        int incrButtonY = sbSize.height - (sbInsets.bottom + incrButtonH);

        /* The thumb must fit within the height left over after we
         * subtract the preferredSize of the buttons and the insets
         * and the gaps
         */
        int sbInsetsH = sbInsets.top + sbInsets.bottom;
        int sbButtonsH = decrButtonH + incrButtonH;
        int gaps = decrGap + incrGap;
        float trackH = sbSize.height - (sbInsetsH + sbButtonsH) - gaps;

        /* Compute the height and origin of the thumb.   The case
         * where the thumb is at the bottom edge is handled specially
         * to avoid numerical problems in computing thumbY.  Enforce
         * the thumbs min/max dimensions.  If the thumb doesn't
         * fit in the track (trackH) we'll hide it later.
         */
        float min = sb.getMinimum();
        float extent = sb.getVisibleAmount();
        float range = sb.getMaximum() - min;
        float value =  sb.getValue();

        int thumbH = (range <= 0)
            ? getMaximumThumbSize().height : (int)(trackH * (extent / range));
        thumbH = Math.max(thumbH, getMinimumThumbSize().height);
        thumbH = Math.min(thumbH, getMaximumThumbSize().height);

        int thumbY = incrButtonY - incrGap - thumbH;
        if (value < (sb.getMaximum() - sb.getVisibleAmount())) {
            float thumbRange = trackH - thumbH;
            thumbY = (int)(0.5f + (thumbRange * ((value - min) / (range - extent))));
            thumbY +=  decrButtonY + decrButtonH + decrGap;
        }

        /* If the buttons don't fit, allocate half of the available
         * space to each and move the lower one (incrButton) down.
         */
        int sbAvailButtonH = (sbSize.height - sbInsetsH);
        if (sbAvailButtonH < sbButtonsH) {
            incrButtonH = decrButtonH = sbAvailButtonH / 2;
            incrButtonY = sbSize.height - (sbInsets.bottom + incrButtonH);
        }
        decrButton.setBounds(itemX, decrButtonY, itemW, decrButtonH);
        incrButton.setBounds(itemX, incrButtonY, itemW, incrButtonH);

        /* Update the trackRect field.
         */
        int itrackY = decrButtonY + decrButtonH + decrGap;
        int itrackH = incrButtonY - incrGap - itrackY;
        trackRect.setBounds(itemX, itrackY, itemW, itrackH);

        /* If the thumb isn't going to fit, zero it's bounds.  Otherwise
         * make sure it fits between the buttons.  Note that setting the
         * thumbs bounds will cause a repaint.
         */
        if(thumbH >= (int)trackH)       {
            if (UIManager.getBoolean("ScrollBar.alwaysShowThumb")) {
                 setThumbBounds(itemX, itrackY, itemW, itrackH);
            } else {
                setThumbBounds(0, 0, 0, 0);
            }
        }
        else {
            if ((thumbY + thumbH) > incrButtonY - incrGap) {
                thumbY = incrButtonY - incrGap - thumbH;
            }
            if (thumbY  < (decrButtonY + decrButtonH + decrGap)) {
                thumbY = decrButtonY + decrButtonH + decrGap + 1;
            }
            setThumbBounds(itemX, thumbY, itemW, thumbH);
        }
    }

        @Override
        protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
            
            g.setColor(Color.white);
            ((Graphics2D) g).drawImage(imageThumb,r.x+r.width-imageTrack.getWidth(null), r.y, imageTrack.getWidth(null), r.height, null);
       
         
        }
        @Override
            protected void paintDecreaseHighlight(Graphics g)
    {
        Insets insets = scrollbar.getInsets();
        scrollbar.setBackground(Color.white);
        Rectangle thumbR = getThumbBounds();
        g.setColor( Color.WHITE);

        if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
            int x = insets.left;
            int y = trackRect.y;
            int w = scrollbar.getWidth() - (insets.left + insets.right);
            int h = thumbR.y - y;
            g.fillRect(x, y, w, h);
        } else {
            int x, w;
            if (scrollbar.getComponentOrientation().isLeftToRight()) {
               x = trackRect.x;
                w = thumbR.x - x;
            } else {
                x = thumbR.x + thumbR.width;
                w = trackRect.x + trackRect.width - x;
            }
            int y = insets.top;
            int h = scrollbar.getHeight() - (insets.top + insets.bottom);
            g.fillRect(x, y, w, h);
        }
    }


        @Override
        protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
            g.setColor(Color.white);
         
            ((Graphics2D) g).drawImage(imageTrack,
                r.x+r.width-imageTrack.getWidth(null), r.y, imageThumb.getWidth(null), r.height, null);
        }
        
       
        @Override
          protected void paintIncreaseHighlight(Graphics g)
    {
        Insets insets = scrollbar.getInsets();
        Rectangle thumbR = getThumbBounds();
        scrollbar.setBackground(Color.white);
        g.setColor(Color.white);

        if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
             int x = insets.left;
            int y = thumbR.y + thumbR.height;
            int w = scrollbar.getWidth() - (insets.left + insets.right);
            int h = trackRect.y + trackRect.height - y;
            g.fillRect(x, y, w, h);
        }
        else {
            int x, w;
            if (scrollbar.getComponentOrientation().isLeftToRight()) {
                x = thumbR.x + thumbR.width;
                w = trackRect.x + trackRect.width - x;
            } else {
                x = trackRect.x;
                w = thumbR.x - x;
            }
            int y = insets.top;
            int h = scrollbar.getHeight() - (insets.top + insets.bottom);
            g.fillRect(x, y, w, h);
        }
    }

       
}

    private static class FauxImage {

        static public Image create(int w, int h, Color c) {
            BufferedImage bi = new BufferedImage(
                w, h, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = bi.createGraphics();
            g2d.setPaint(c);
            g2d.fillRect(0, 0, w, h);
            g2d.dispose();
            return bi;
        }
    }



private static class happen implements ChangeListener{

       @Override
        public void stateChanged(ChangeEvent e) {
            JRadioButton radio =(JRadioButton)e.getSource();
        if(radio.isSelected()){
        int iii = Integer.valueOf(radio.getName());
        unquest.setText(unquest.getText().replaceFirst(" "+iii+" "," "));
            tib[iii]=radio.getIconTextGap();
            bit[iii]= Integer.valueOf(radio.getActionCommand());
            synchronized(subjects.changed){
            subjects.changed.notifyAll();
                     
                   }
        
        
        
        } }
    }
}
